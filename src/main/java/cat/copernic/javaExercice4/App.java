package cat.copernic.javaExercice4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.stage.Modality;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene4;
    private static Stage stage4;

    @Override
    public void start(Stage stage) throws IOException {
        
      
        // Task 4
        stage4 = new Stage();
        scene4 = new Scene(loadFXML("fourth"));
        stage4.setTitle("Exercice 4");
        stage4.initModality(Modality.NONE);
        stage4.setScene(scene4);
        stage4.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene4.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}