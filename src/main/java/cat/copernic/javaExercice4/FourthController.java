package cat.copernic.javaExercice4;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;

import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Adria & Manel
 */
public class FourthController implements Initializable {

    
    private final Double MAX_VALUE_SLIDER = 250.0;
    
    
    //<editor-fold defaultstate="collapsed" desc="Parametres directament inicialitzats des del fitxer primary.fxml">
    @FXML
    private TextField txtm1;
    
    @FXML
    private TextField txtm2;
    
    @FXML
    private ProgressBar progress1;
    
    @FXML
    private ProgressBar progress2;
    
    @FXML
    private ProgressIndicator progress3;
    
    @FXML
    private Button btn_start;
    
    @FXML
    private Button btn_stop;
    
    @FXML
    private Slider slider1;
    
    @FXML
    private Slider slider2;
    
    @FXML
    private HBox zona_rigth;
    
    @FXML
    private VBox zona_up;
    
    @FXML
    private HBox zona_left;
    
    @FXML
    private RadioButton rb1;

    @FXML
    private RadioButton rb3;

    @FXML
    private RadioButton rb2;

    @FXML
    private RadioButton rb4;
    
    @FXML
    private ToggleGroup toggle1;
    
    @FXML
    private VBox vBoxRadios;
    
//</editor-fold>


    /**
     * Si implementem aquest mètode, llavors s'executa al iniciar el controlador
     * És ideal per a inicialitzar l'estat de la pantalla, etc...
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // sliders
        slider1.setMax(MAX_VALUE_SLIDER);
        slider2.setMax(MAX_VALUE_SLIDER);
        
        //barres de progres
        setProgres();
        
        // botons i sliders desactivats al principi
        this.readyStart(true);
        
        // EVENTS:
        // Podem fer-ho de dues maneres:
        // 1 - Establim el comportament del botó programáticament (exemple: botó STOP)
        btn_stop.setOnAction(event -> onAction_btn_stop(event));
        // 2 - Ho definim al paràmetre onAction="#onAction_btn_start" del fitxer FXML (exemple: botó START)
        
        
        // establim el comportament dels sliders mitjançant programació
        slider1.valueProperty().addListener( (ObservableValue <? extends Number > observable, Number oldValue, Number newValue) -> setProgres() );
        slider2.valueProperty().addListener( (ObservableValue <? extends Number > observable, Number oldValue, Number newValue) -> setProgres() );
        
        // Generate the custom toggle group and add the toggleButtons
        generateTroggleGroup();
        
        // add a change listener 
        toggle1.selectedToggleProperty().addListener(new ChangeListener<Toggle>()  
        { 
            public void changed(ObservableValue<? extends Toggle> ob, Toggle o, Toggle n) 
            { 
  
                RadioButton rb = (RadioButton)toggle1.getSelectedToggle();
  
                if (rb != null) {
                    
                    System.out.println(rb.getId());
                    
                    switch (rb.getId()){
                        
                        case "rb1" ->  {
                             setSliderValue(slider1, 0f);
                             setSliderValue(slider2, 0f);
                        }
                        case "rb2" ->  {
                            setSliderValue(slider1, getSliderPercentage(slider1, 25));
                            setSliderValue(slider2, getSliderPercentage(slider2, 25));
                        }
                        case "rb3" ->  {
                            setSliderValue(slider1, getSliderPercentage(slider1, 50));
                            setSliderValue(slider2, getSliderPercentage(slider2, 50));
                        }
                        case "rb4" -> {
                            setSliderValue(slider1, getSliderPercentage(slider1, 100));
                            setSliderValue(slider2, getSliderPercentage(slider2, 100));
                        }
                    }
                } 
            } 
        }); 
    }    

    /***
     * El comportament de boto ve definit al fitxer FXML
     * @param event 
     */
    @FXML
    private void onAction_btn_start(ActionEvent event) {
        
        // canviem estat de la pantalla ready stop
        this.readyStart(false);
    }
    
    private void onAction_btn_stop(ActionEvent event) {
        
        //posem comptadors a zero
        resetAll();
        
        // canviem estat de la pantalla ready stop
        this.readyStart(true);
    }
    
    /***
     * Activa o desactiva els controls de la pantalla
     * 
     * @param disable 
     */
    private void readyStart(boolean disable)
    {
        zona_rigth.setDisable(disable);
        zona_up.setDisable(disable);
        zona_left.setDisable(disable);
        btn_start.setDisable(! disable);
        btn_stop.setDisable(disable);
        vBoxRadios.setDisable(disable);
        
    }
    
    /***
     * Posa tots els sliders a zero
     */
    private void resetAll()
    {
        slider1.setValue(0);
        slider2.setValue(0);
    }
    
    /***
     * Actualitza les barres de progrés i els camps de text en funció dels sliders
     * 
     * @param p1
     * @param p2
     * @return 
     */
    private void setProgres()
    {
        // recollim els valors actuals dels sliders
        Double s1 = slider1.getValue();
        Double s2 = slider2.getValue();
        
        //actualitzem camps de text
        txtm1.setText(String.format("%,.2f",s1));
        txtm2.setText(String.format("%,.2f",s2));
        
        //actualitzem les barres de progrés
        progress1.setProgress(s1 / MAX_VALUE_SLIDER);
        progress2.setProgress(s2 / MAX_VALUE_SLIDER);
        progress3.setProgress(( s1+s2) / (MAX_VALUE_SLIDER*2) );
        
        //Change color ProgressIndicator/ProgressBar < 50% && > 50% && > 90%
        setColorProgressIndicator(progress3, s1, s2);
        setColorProgressBar(progress1, s1);
        setColorProgressBar(progress2, s2);
    }

    
    // Ex 5. part 2
    // Manage Left Slider
    @FXML
    private void setLeftSlider() {
        Float textBoxValue = Float.parseFloat(txtm1.getText());
        if(checkSliderValue(slider1, textBoxValue)) {
            setSliderValue(slider1, textBoxValue);
        }
    }
    
    // Manage Right Slider
    @FXML
    private void setRightSlider() {
        Float textBoxValue = Float.parseFloat(txtm2.getText());
        if(checkSliderValue(slider2, textBoxValue)) {
            setSliderValue(slider2, textBoxValue);
        }
    }

    // Check if a value is >= or <= than a given value
    private boolean checkSliderValue(Slider slider, Float textBoxValue) {
        return textBoxValue >= slider.getMin() && textBoxValue <= slider.getMax();
    }

    // Set a slider value
    private void setSliderValue(Slider slider, Float value) {
        slider.setValue(value);
    }
    
    
    // Ex 5. part 3
    
    ToggleGroup toggleGroup;
    
    private void generateTroggleGroup() {
        toggleGroup = new ToggleGroup();
    }

    // Get the specific pergentage of a slider max value
    private float getSliderPercentage(Slider slider, float percentage) {
        return (float) (slider.getMax() * percentage / 100);
    }
    
    private void setColorProgressBar(ProgressBar progress, Double s){
        if(s < (MAX_VALUE_SLIDER*50)/100){
            progress.setStyle(" -fx-accent: green;");
        } if(s >= (MAX_VALUE_SLIDER*50)/100){
            progress.setStyle(" -fx-accent: #ffa500;");
        } if(s > (MAX_VALUE_SLIDER*90)/100){
            progress.setStyle(" -fx-accent: red;");
        }
    }
    
    private void setColorProgressIndicator(ProgressIndicator progress, Double s1, Double s2){
        if((s1+s2) <= (((MAX_VALUE_SLIDER*2)*50)/100)) {
            progress.setStyle(" -fx-progress-color: green;");
        } if((s1+s2) >= (((MAX_VALUE_SLIDER*2)*50)/100)){
            progress.setStyle(" -fx-progress-color: #ffa500;");
        } if((s1+s2) > (((MAX_VALUE_SLIDER*2)*90)/100)){
            progress.setStyle(" -fx-progress-color: red;");
        }
    }

}
